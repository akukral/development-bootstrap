# Intro #
This is a Grunt, Bower, Twitter Bootstrap rapid development without all the cruft of Yeoman. For folks that one one directory and easy deployment.

After cloning the repo command line:

```
#!

$npm install
$grunt install
```

# Info #

Includes:

* Twitter Bootstrap
* Font Awesome
* JQuery
* Modernizer
* Grunt: Concat, Uglify, Less, Imagemin, Express, Open, Watch, Dploy

# Functions #

Beyond what each Grunt task is capable of doing. There are 2 main tasks.

**Grunt server** will start a js server, open the index.html page and watch it for livereload
```
#!

$grunt server
```

**Grunt Build** will start a contact, uglify, render less, and upload all pertinant files to the dploy server
```
#!

$grunt build
```


