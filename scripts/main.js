function random(array){
	return array[Math.floor(Math.random() * array.length)];
}
function shuffle(array){
	return array.sort(function(){ return Math.random() - 0.5;});
}
function log(){
	var args = Array.prototype.slice.call(arguments);
	if(args[0]==='alert'){
		args.splice(0,1);
		alert(args);
		window.console.log.apply(console, args);
	}else{
		args.unshift('Site: ');
		window.console.log.apply(console, args);
	}
}
String.prototype.trim = function(){return this.replace(/^\s+|\s+$/g, "");};

var SiteDetails = {
					name:"New Site",version:"2.2"
				};
log(SiteDetails);
// var myvar = "Hello";
// console.info("Logged!");
// console.warn("Logged!");
// console.error("Logged!");

$(document).ready(function() {
	log('Document Ready');
	$('.browsehappy a, .browsehappy small').click(function(event,index) {
		/* Act on the event */
		$(this).attr('target','_blank').parent().remove();
	});
});
