var path = require('path');

module.exports = function(grunt) {

    // 1. All configuration goes here
    var pathingConfig = {
        development: 'dev', //<%= yeoman.app %>, {.tmp,<%= yeoman.app %>}
        production: 'live'
    };

    grunt.initConfig({
        yeoman: pathingConfig,
        // 2. Configuration for concatinating files goes here.
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            build: {
                files: [
                    {   src: [
                        'bower_components/modernizr/modernizr.js'
                        ],
                        dest: '.tmp/scripts/vendor/modernizr.js'
                    },
                    {   src: [
                        'bower_components/bootstrap/js/affix.js',
                        'bower_components/bootstrap/js/alert.js',
                        'bower_components/bootstrap/js/dropdown.js',
                        'bower_components/bootstrap/js/tooltip.js',
                        'bower_components/bootstrap/js/modal.js',
                        'bower_components/bootstrap/js/transition.js',
                        'bower_components/bootstrap/js/button.js',
                        'bower_components/bootstrap/js/popover.js',
                        'bower_components/bootstrap/js/typeahead.js',
                        'bower_components/bootstrap/js/carousel.js',
                        'bower_components/bootstrap/js/scrollspy.js',
                        'bower_components/bootstrap/js/collapse.js',
                        'bower_components/bootstrap/js/tab.js'
                        ],
                        dest: '.tmp/scripts/vendor/bootstrap.js'
                    },
                    {   src: [
                        'bower_components/jquery/dist/jquery.min.js',
                        'scripts/main.js'
                        ],
                        dest: '.tmp/scripts/main.js'
                    }
                ]
            },
            dev: {
                files: [
                    {   src: [
                        'bower_components/modernizr/modernizr.js'
                        ],
                        dest: '.tmp/scripts/vendor/modernizr.js'
                    },
                    {   src: [
                        'bower_components/bootstrap/js/affix.js',
                        'bower_components/bootstrap/js/alert.js',
                        'bower_components/bootstrap/js/dropdown.js',
                        'bower_components/bootstrap/js/tooltip.js',
                        'bower_components/bootstrap/js/modal.js',
                        'bower_components/bootstrap/js/transition.js',
                        'bower_components/bootstrap/js/button.js',
                        'bower_components/bootstrap/js/popover.js',
                        'bower_components/bootstrap/js/typeahead.js',
                        'bower_components/bootstrap/js/carousel.js',
                        'bower_components/bootstrap/js/scrollspy.js',
                        'bower_components/bootstrap/js/collapse.js',
                        'bower_components/bootstrap/js/tab.js'
                        ],
                        dest: '.tmp/scripts/vendor/bootstrap.js'
                    },
                    {   src: [
                        'bower_components/jquery/dist/jquery.min.js',
                        'scripts/main.js'
                        ],
                        dest: '.tmp/scripts/main.js'
                    }
                ]
            }
        },
        uglify: {
            dev:{
                options: {
                    mangle: false,
                    compress: false,
                    beautify: true,
                    preserveComments: true
                },
                files: {
                    'scripts/vendor/modernizr.min.js': ['.tmp/scripts/vendor/modernizr.js'],
                    'scripts/vendor/bootstrap.min.js': ['.tmp/scripts/vendor/bootstrap.js'],
                    'scripts/main.min.js': ['.tmp/scripts/main.js']
                }
            },
            build: {
                // options: {
                //     banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                // },
                files: {
                    'scripts/vendor/modernizr.min.js': ['.tmp/scripts/vendor/modernizr.js'],
                    'scripts/vendor/bootstrap.min.js': ['.tmp/scripts/vendor/bootstrap.js'],
                    'scripts/main.min.js': ['.tmp/scripts/main.js']
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'images/build/'
                }]
            }
        },
        less: {
            style: {
                files: {
                    "styles/main.css": "styles/main.less"
                },
                options: {
                    compress: false,
                    yuicompress: false,
                    cleancss: false,
                    optimization: 2
                }
            },
            build: {
                files: {
                    "styles/main.css": "styles/main.less"
                },
                options: {
                    compress: true,
                    yuicompress: true,
                    cleancss: true,
                    optimization: 2
                }
            }
        },
        watch: {
            options: {
                livereload: true,
                options: { interval: 3500 }
            },
            templates: {
                files: '*.html',
                options: {
                    livereload: true,
                }
            },
            js: {
                files: ['scripts/main.js'],
                tasks: ['concat:dev','uglify:dev'],
                options: {
                    livereload: true,
                }
            },
            css: {
                files: ['styles/*.less'],
                tasks: ['less:style'],
                options: {
                    livereload: true,
                }
            }
        },
        open : {
            file : {
                 path: 'http://localhost:<%= express.all.options.port%>'
            }
        },
        express: {
            all: {
                options: {
                    port: 8888,
                    hostname: "0.0.0.0",
                    bases: [path.resolve('./')]
                }
            }
        },
        useminPrepare: {
            html: 'index.html',
            options: {
                dest:'./'
            }
        },
        dploy: {
            production: {
                scheme: "ftp",
                host: "sitename.com",
                user: "user",
                port: "21",
                pass: "password",
                path:{
                    remote: "/path/to/site/root/"
                }
            }
        }
    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-open');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-dploy');


    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('build', ['concat:build','uglify:build','less:build']);

    grunt.registerTask('server', ['express','open','watch']);

};